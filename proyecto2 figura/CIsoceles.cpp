//
//  CIsoceles.cpp
//  poo211
//
//  Created by Ahgala on 10/11/20.
//

#include "CIsoceles.hpp"


CIsoceles::CIsoceles():CTriangulo(){
  // No hace nada especial
}

CIsoceles::CIsoceles(float ladoIgual, float ladoDesigual):CTriangulo(ladoIgual,ladoIgual,ladoDesigual){
  // No hace nada especial
}

CIsoceles::CIsoceles(float ladoIgual, float ladoDesigual,float altura):CTriangulo(ladoIgual,ladoIgual,ladoDesigual,altura){
  // No hace nada especial
}

void CIsoceles::setLado(int lado,float valor){
  if((lado >=1)&&(lado<=2)){
     CTriangulo::setLado(1, valor);
     CTriangulo::setLado(2, valor);
  }
  if((lado >=3)&&(lado<=4)) CTriangulo::setLado(lado, valor);
}

