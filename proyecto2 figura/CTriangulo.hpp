//
//  CTriangulo.hpp
//  poo211
//
//  Created by Ahgala on 09/11/20.
//

#ifndef CTriangulo_hpp
#define CTriangulo_hpp

#include "CFigura.hpp"

class CTriangulo: public CFigura {
private:
  float mLado1, mLado2, mLado3;
  float mAltura;
public:
  CTriangulo();
  CTriangulo(float,float,float);
  CTriangulo(float,float,float,float);
  float perimetro(void);
  float area(void);
  
  void setLado(int,float);
  float getLado(int);
};

#endif /* CTriangulo_hpp */

