//
//  CTriangulo.cpp
//  poo211
//
//  Created by Ahgala on 09/11/20.
//

#include "CTriangulo.hpp"

CTriangulo::CTriangulo():CFigura(3){
  mLado1=0;
  mLado2=0;
  mLado3=0;
  mAltura=0;
}

CTriangulo::CTriangulo(float lado1,float lado2,float lado3):CFigura(3){
  mLado1=lado1;
  mLado2=lado2;
  mLado3=lado3;
  mAltura=((lado2+lado3)/2);
}

CTriangulo::CTriangulo(float lado1,float lado2,float lado3,float altura):CFigura(3){
  mLado1=lado1;
  mLado2=lado2;
  mLado3=lado3;
  mAltura=altura;
}

float CTriangulo::perimetro(){
  return mLado1+mLado2+mLado3;
}

float CTriangulo::area(){
  return mLado1*mAltura/2;
}

void CTriangulo::setLado(int lado,float valor){
  switch (lado) {
    case 1:
      mLado1=valor;
      break;
    case 2:
      mLado2=valor;
      break;
    case 3:
      mLado3=valor;
      break;
    case 4:
      mAltura=valor;
      break;
    default:
      break;
  }
}

float CTriangulo::getLado(int lado){
  switch (lado) {
    case 1:
      return mLado1;
    case 2:
      return mLado2;
    case 3:
      return mLado3;
    case 4:
      return mAltura;
    default:
      return 0;
  }
}

