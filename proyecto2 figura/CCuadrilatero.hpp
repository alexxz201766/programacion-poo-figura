//
//  CCuadrilatero.hpp
//  proyecto2 figura
//
//  Created by ALEJANDRO TOLENTINO on 18/11/20.
//

#ifndef CCuadrilatero_hpp
#define CCuadrilatero_hpp
#include "CFigura.hpp"
#include <stdio.h>

class CCuadrilatero: public CFigura {
private:
    float cuadri1,cuadri2,cuadri3,cuadri4;
    float cuadrialtura;
public:
    CCuadrilatero();
    CCuadrilatero(float,float,float,float);
    CCuadrilatero(float,float,float,float,float);
    
    float perimetro(void);
    float area(void);
    
    void setLado(int,float);
    float getLado(int);

};

#endif /* CCuadrilatero_hpp */
