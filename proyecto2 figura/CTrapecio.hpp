//
//  CTrapecio.hpp
//  proyecto2 figura
//
//  Created by ALEJANDRO TOLENTINO on 19/11/20.
//

#ifndef CTrapecio_hpp
#define CTrapecio_hpp
#include "CCuadrilatero.hpp"
#include <stdio.h>

class CTrapecio: public CCuadrilatero {
private:
    float tra1,tra2,tra3,tra4;
    float trapaltura;
public:
    CTrapecio();
    CTrapecio(float,float,float,float);
    CTrapecio(float,float,float,float,float);
   
    float perimetro(void);
    float area(void);
    
    void setLado(int,float);
    float getLado(int);
};
#endif /* CTrapecio_hpp */
