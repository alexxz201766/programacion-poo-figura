//
//  CFigura.hpp
//  poo211
//
//  Created by Ahgala on 09/11/20.
//

#ifndef CFigura_hpp
#define CFigura_hpp

class CFigura { // polígonos regulares
private:
  int mNumLados;
public:
  CFigura();
  CFigura(int);
  float perimetro(void);
  float area(void);
  void setNumLados(int);
  int  getNumLados(void);
  void setLado(int,float);
  float getLado(int);
};

#endif /* CFigura_hpp */

