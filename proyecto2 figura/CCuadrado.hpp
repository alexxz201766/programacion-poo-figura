//
//  CCuadrado.hpp
//  proyecto2 figura
//
//  Created by ALEJANDRO TOLENTINO on 19/11/20.
//

#ifndef CCuadrado_hpp
#define CCuadrado_hpp
#include "CRectanguno.hpp"
#include <stdio.h>

class CCuadrado: public CRectanguno {
  
 private:
    float cuadro1,cuadro2,cuadro3,cuadro4;
    float cuadroaltura;
public:
    CCuadrado();
    CCuadrado(float);
    CCuadrado(float,float);
    void setLado(int,float);
};

#endif /* CCuadrado_hpp */
