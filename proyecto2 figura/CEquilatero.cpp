//
//  CEquilatero.cpp
//  poo211
//
//  Created by Ahgala on 10/11/20.
//

#include "CEquilatero.hpp"

CEquilatero::CEquilatero():CTriangulo(){
  // No hace nada especial
}

CEquilatero::CEquilatero(float lado):CTriangulo(lado,lado,lado){
  // No hace nada especial
}

CEquilatero::CEquilatero(float lado,float altura):CTriangulo(lado,lado,lado,altura){
  // No hace nada especial
}

void CEquilatero::setLado(int lado,float valor){
  if((lado >=1)&&(lado<=3)){
     CTriangulo::setLado(1, valor);
     CTriangulo::setLado(2, valor);
     CTriangulo::setLado(3, valor);
  }
  if(lado==4) CTriangulo::setLado(lado, valor);
}

