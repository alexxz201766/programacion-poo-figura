//
//  main.cpp
//  poo211
//
//

#include <iostream>
#include "CEquilatero.hpp"
#include "CIsoceles.hpp"
#include "CCuadrilatero.hpp"
#include "CTrapecio.hpp"
#include "CParalelogramo.hpp"
#include "CRectanguno.hpp"
#include "CCuadrado.hpp"

using namespace std;

int main(void) {

  CIsoceles f(5,8,11);

  f.setLado(1, 3);
  f.setLado(2,10);
  f.setLado(3,10);
  f.setLado(4,100);

  cout << "El número de lados del triángulo F es: " << f.getNumLados() << "\n";
  cout << "El área del triángulo F es: " << f.area() << "\n";
  cout << "El perímetro del triángulo F es: " << f.perimetro()<< "\n";

  cout << "El lado 1 mide: "<< f.getLado(1)<< ", el lado 2 mide: "<< f.getLado(2)<< ", el lado 3 mide: "<< f.getLado(3) << " y la altura mide: " << f.getLado(4) << " del triángulo F.\n\n";
    cout << "----------------------------------------------------""\n";
    cout << "EL CUADRILATERO: EN GENERAL ES LA FIGURA QUE TIENE 4 LADOS""\n\n";
    
    CCuadrilatero G (5,5,5,5);
    
    cout << "El número de lados del cuadrado g es: " << G.getNumLados() << "\n";
    cout << "El área del cuadrado G es: " << G.area() << "\n";
    cout << "El perímetro del cuadrado g es: " << G.perimetro()<< "\n";

    cout << "El lado 1 mide: "<< G.getLado(1)<< ", el lado 2 mide: "<< G.getLado(2)<< ", el lado 3 mide: "<< G.getLado(3)<< ",el lado 4 mide: "<< G.getLado(4)<<" y la altura mide: " << G.getLado(5) << " cel cuadrado g.\n\n";
    cout << "----------------------------------------------------""\n";
    cout << "TRAPECIO: EL TRAPECIO CONSTA DE 4 LADOS DONDE INTEGRA UN LADO B MAYOR Y b MENOR, DONDE B MAYOR SERA LA BASE. LOS OTROS DOS LADOS SERAN LAS DIAGONALES""\n\n";
   
    CTrapecio J(10,4,5,5);
    cout << "El número de lados del trapecio J es: " << J.getNumLados() << "\n\n";
    
    cout << "El lado B mayor mide: "<< J.getLado(1) << "\n";
    cout << "El lado b menor mide: "<< J.getLado(2) << "\n";
    cout << " ----para calcular la altura se emplea la formula n= (B-b)/2. despues altura sera igual a la raiz de uno de sus lados diagonales al cuadrado menos n elevadaal cuadrado---- ""\n";
    cout << "El lado de la diagonal 1 mide: "<< J.getLado(3) << "\n";
    cout << "El lado de la diagonal 2 mide: "<< J.getLado(4) << "\n";
    cout << "Empleando la fomula anteriormente dicha, la altura tendra un valor de: " << J.getLado(5) << "\n\n";
    
   
    cout << "El área del trapecio J es: " << J.area() << "\n";
    cout << "El perímetro del trapecio J es: " << J.perimetro()<< "\n\n";
 
    cout << "----------------------------------------------------" "\n";
    cout << "PARALELOGRAMO:LOS LADOS OPUESTOS DE UN PARALELOGRAMO SON PARALELOS""\n\n";
   
    CParalelogramo T(2,3,2,3);
  
    cout << "El número de lados del paralelogramo T es: " << T.getNumLados() << "\n\n";
   
    cout << "El lado 1 mide: "<< T.getLado(1)<< "\n";
    cout << "el lado 2 mide: "<< T.getLado(2)<< "\n";
    cout << "el lado 3 mide: "<< T.getLado(3)<< "\n";
    cout << "el lado 4 del paralelogramo mide: "<< T.getLado(4) << "\n\n";
    cout << "El área del paralelogramo es: " << T.area() << "\n";
    cout << "El perímetro del paralelogramo T es: " << T.perimetro()<< "\n";

    cout << "y la altura mide: " << T.getLado(5) << " del paralelogramo T.\n\n";
    
    cout << "----------------------------------------------------" "\n";
    cout << "RECTANGULO: ES UNA FIGURA QUE TIENE 4 LADOS, DOS DE LA MISMA MEDIDA Y DOS DE DE MEDIDAN IGUAL PERO DIFERENTE A LAS PRIMERAS""\n\n";
    
    CRectanguno K(4,5,4,5);
   
   cout << "El número de lados del rectangulo K es: " << K.getNumLados() << "\n\n";
   
    cout << "El lado 1 mide: "<< K.getLado(1)<< "\n";
    cout << "el lado 2 mide: "<< K.getLado(2)<< "\n";
    cout << "el lado 3 mide: "<< K.getLado(3)<< "\n";
    cout << "el lado 4 del rectangulo mide: "<< K.getLado(4) << "\n\n";
    cout << "El área del rectangulo es: " << K.area() << "\n";
    cout << "El perímetro del rectangulo F es: " << K.perimetro()<< "\n";

    cout << "y la altura mide: " << K.getLado(5) << " del rectangulo K.\n\n";
    
    cout << "----------------------------------------------------" "\n";
    cout << "CUADRADO: ES UNA FIGURA QUE SUS 4 LADOS MIDEN IGUAL""\n\n";
    
    CCuadrado Y(3);
    
    cout << "El número de lados del cuadrado Y es: " << Y.getNumLados() << "\n\n";
   
    cout << "El lado 1 mide: "<< Y.getLado(1)<< "\n";
    cout << "el lado 2 mide: "<< Y.getLado(2)<< "\n";
    cout << "el lado 3 mide: "<< Y.getLado(3)<< "\n";
    cout << "el lado 4 del cuadrado Y mide: "<< Y.getLado(4) << "\n\n";
    cout << "El área del cuadrado Y es: " << Y.area() << "\n";
    cout << "El perímetro del cuadrado Y es: " << Y.perimetro()<< "\n";

    cout << "y la altura mide: " << Y.getLado(5) << " del cuadrado.\n\n";
    cout << "----------------------------------------------------" "\n\n";

    
    return 0;
}

