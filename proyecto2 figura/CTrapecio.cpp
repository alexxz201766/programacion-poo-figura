//
//  CTrapecio.cpp
//  proyecto2 figura
//
//  Created by ALEJANDRO TOLENTINO on 19/11/20.
//
#include <math.h>
#include <iostream>
#include "CTrapecio.hpp"

CTrapecio::CTrapecio():CCuadrilatero(){
    tra1=0;
    tra2=0;
    tra3=0;
    tra4=0;
    trapaltura=0;
}

CTrapecio::CTrapecio(float ladoo1,float ladoo2,float ladoo3,float ladoo4) : CCuadrilatero(ladoo1,ladoo1,ladoo2,ladoo3){
    tra1=ladoo1;
    tra2=ladoo2;
    tra3=ladoo3;
    tra4=ladoo4;
    trapaltura=((ladoo1-ladoo2)/2);
}
CTrapecio::CTrapecio(float ladoo1,float ladoo2,float ladoo3,float ladoo4,float altura) : CCuadrilatero(ladoo1,ladoo2,ladoo3,ladoo4,altura){
    tra1=ladoo1;
    tra2=ladoo2;
    tra3=ladoo3;
    tra4=ladoo4;
    trapaltura=altura;
}
float CTrapecio::perimetro(){
    return tra1+tra2+tra3+tra4;
}
float CTrapecio::area(){
    return sqrt(pow(tra3, 2)-pow(trapaltura, 2));
}
void CTrapecio::setLado(int lado,float valor){
    switch (lado) {
      case 1:
        tra1=valor;
        break;
      case 2:
        tra2=valor;
        break;
      case 3:
        tra3=valor;
        break;
      case 4:
        tra4=valor;
        break;
        case 5:
            trapaltura=valor;
            break;
      default:
        break;
    }
}
float CTrapecio::getLado(int lado){
      switch (lado) {
        case 1:
          return tra1;
        case 2:
          return tra2;
        case 3:
          return tra3;
        case 4:
          return tra4;
          case 5:
              return trapaltura;
        default:
          return 0;
      }
}
