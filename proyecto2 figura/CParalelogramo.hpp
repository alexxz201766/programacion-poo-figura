//
//  CParalelogramo.hpp
//  proyecto2 figura
//
//  Created by ALEJANDRO TOLENTINO on 19/11/20.
//

#ifndef CParalelogramo_hpp
#define CParalelogramo_hpp
#include "CCuadrilatero.hpp"
#include <stdio.h>

class CParalelogramo: public CCuadrilatero {
private:
    float cuaa1,cuaa2,cuaa3,cuaa4;
    float cualtura;
public:
    CParalelogramo();
    CParalelogramo(float,float,float,float);
    CParalelogramo(float,float,float,float,float);
    
   // float area(void);
    void setLado(int,float);
};
#endif /* CParalelogramo_hpp */
