//
//  CRectanguno.hpp
//  proyecto2 figura
//
//  Created by ALEJANDRO TOLENTINO on 19/11/20.
//

#ifndef CRectanguno_hpp
#define CRectanguno_hpp

#include <stdio.h>
#include "CParalelogramo.hpp"

class CRectanguno: public CParalelogramo {
private:
    float rec1,rec2,rec3,rec4;
    float raltura;
public:
    CRectanguno();
    CRectanguno(float,float,float,float);
    CRectanguno(float,float,float,float,float);
    
    void setLado(int,float);
};

#endif /* CRectanguno_hpp */
